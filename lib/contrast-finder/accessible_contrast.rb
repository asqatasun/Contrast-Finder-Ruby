# Module to check whether the contrast between two colors is accessible or not
module ContrastFinder
  require 'chroma'
  require 'chroma_wcag_contrast'
  require_relative 'ratio'

  ACCEPTABLE_RATIO = [RATIO_3, RATIO_4_5, RATIO_7].freeze

  # Constants are from the W3C formula
  #  https://www.w3.org/TR/WCAG20/#contrast-ratiodef
  #  https://www.w3.org/TR/WCAG20/#relativeluminancedef
  RED_FACTOR            = 0.2126
  GREEN_FACTOR          = 0.7152
  BLUE_FACTOR           = 0.0722
  CONTRAST_FACTOR       = 0.05
  RGB_MAX_VALUE         = 255
  SRGB_FACTOR           = 0.03928
  LUMINANCE_INF         = 12.92
  LUMINANCE_SUP_CONST   = 0.055
  LUMINANCE_SUP_CONST2  = 1.055
  LUMINANCE_EXP         = 2.4

  # For a given ratio, says if the contrast between two colors is accessible or
  # not
  #
  # @param color1 [String] color in hexadecimal (eg #468847)
  # @param color2 [String] color in hexadecimal (eg #DFF0D8)
  # @return [Boolean] whether the contrast is accessible or not
  def self.accessible_contrast?(color1, color2, minimum_ratio = RATIO_4_5)
    unless ACCEPTABLE_RATIO.include?(minimum_ratio)
      raise(Exception, 'Invalid ratio')
    end

    # TODO: check color1 and color2 validity

    ratio = ChromaWcagContrast.ratio(
      Chroma.paint(color1),
      Chroma.paint(color2)
    )
    ratio >= minimum_ratio ? true : false
  end
end
