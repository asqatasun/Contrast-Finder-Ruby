require_relative '../lib/contrast-finder/version'
require_relative '../lib/contrast-finder/accessible_contrast'

# ContrastFinder
# Find correct color contrasts for web accessibility.
module ContrastFinder
  class <<self
    # include AccessibleContrast

    def contrast_finder(text_color, background_color, _ratio = RATIO_4_5)
      txt_color = Chroma.paint(text_color)
      bg_color = Chroma.paint(background_color)
      return 'Contrast is accessible' if accessible_contrast?(txt_color.to_hex,
                                                              bg_color.to_hex)
      # Find good colors for contrast respecting to ratio
      # 1) First try to move the L (luminance) cursor
      # 2) Then try to move the S (saturation) cursor
      # 3) Finally, try to move the H (hue) cursor, but just a little as it
      # changes the color

      # Part 1: vary Luminance
      # TODO: refactor the while loop with blocks
      while txt_color.hsl.l <= 100
        txt_color.lighten(1)
        if accessible_contrast?(txt_color, bg_color)
          puts 'Text color RGB: ' + txt_color.to_hex(true)
          puts 'Text color HSL: ' + txt_color.to_hsl
        end
      end
    end
  end
end

# ContrastFinder.contrast_finder('#468847', '#DFF0D8', '4.5')