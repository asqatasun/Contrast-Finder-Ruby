# Contributing to Contrast-Finder

:+1: First off, thanks for taking the time to contribute! :+1:

We are really glad to have you on board !
You can help in many ways:

1. Use Contrast-Finder !
1. Help translate Contrast-Finder
1. Give us [feedback on the forum](https://forum.asqatasun.org/c/contrast-finder) or [fill in bug report](https://gitlab.com/asqatasun/Contrast-Finder-Ruby/issues)

[Merge Requests](https://gitlab.com/asqatasun/Contrast-Finder-Ruby/issues) are warmly welcome!


## Fill in bug reports

[Fill in a bug report](https://gitlab.com/asqatasun/Contrast-Finder-Ruby/issues)

* Please do not assign issue to anyone.
* You may assign an issue to yourself, meaning to others "I'm actually working on this issue".
* When closing an issue, please *always* add a comment explaining why you are closing it.

## Sources of inspiration for CONTRIBUTING.md

* For contributors doc [Gitlab Workflow](https://about.gitlab.com/handbook/#gitlab-workflow)
* [Atom's Contributing file](https://github.com/atom/atom/blob/master/CONTRIBUTING.md) that is really good and brightly written.