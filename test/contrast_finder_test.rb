require 'test/unit'
require 'contrast_finder'

# Test 'contrast_finder'
class AccessibleContrastTest < Test::Unit::TestCase
  def test_contrast_finder_exits
    assert_equal 'Contrast is accessible',
                 ContrastFinder.contrast_finder('#0055AA',
                                                '#FFFFA5')
  end
end
