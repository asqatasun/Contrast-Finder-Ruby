require 'test/unit'
require 'contrast-finder/accessible_contrast'

# Test 'accessible_contrast?'
class AccessibleContrastTest < Test::Unit::TestCase
  def test_validness_of_ratio
    # refute_nil ::AccessibleContrast::RATIO_3
    refute_nil ContrastFinder::RATIO_3
  end

  # Test accessible_contrast? with triples of values (text color, background
  # color and ratio)
  # known to be not accessible
  def test_contrast_not_accessible
    # Green / green used from the beginning
    assert_false ContrastFinder.accessible_contrast?('#468847', '#DFF0D8', 4.5)
    assert_false ContrastFinder.accessible_contrast?('#468847', '#DFF0D8', 7)

    # Orange / white from Open-S
    assert_false ContrastFinder.accessible_contrast?('#EF7000', '#FFFFFF', 4.5)

    # Dark Grey / Light grey from link counter in Discourse
    assert_false ContrastFinder.accessible_contrast?('#7A7A7A', '#E4E4E4', 4.5)

    # Other colors
    assert_false ContrastFinder.accessible_contrast?('#8B98B8', '#FAFAFA', 4.5)
  end

  # Test accessible_contrast? with triples of values (text color, background
  # color and ratio)
  # known to be actually accessible
  def test_contrast_accessible
    # Green / green used from the beginning
    assert_true ContrastFinder.accessible_contrast?('#468847', '#DFF0D8', 3)

    # Orange / white from Open-S
    assert_true ContrastFinder.accessible_contrast?('#EF7000', '#FFFFFF', 3)

    # Blue / Yellow from Amstrad CPC64 ;)
    assert_true ContrastFinder.accessible_contrast?('#0055AA', '#FFFFA5', 4.5)
  end
end
